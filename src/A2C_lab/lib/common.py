import sys
import time
import numpy as np

import torch
import torch.nn as nn


class RewardTrackerTest:
    def __init__(self, agent, stop_reward):

        self.stop_reward = stop_reward
        self.agent = agent

    def __enter__(self):
        self.ts = time.time()
        self.ts_frame = 0
        self.total_rewards = []
        return self

    def __exit__(self, *args):
        print("finish")

    def reward(self, reward, frame, epsilon=None):
        self.total_rewards.append(reward)
        speed = (frame - self.ts_frame) / (time.time() - self.ts)
        self.ts_frame = frame
        self.ts = time.time()
        mean_reward = np.mean(self.total_rewards[-100:])
        epsilon_str = "" if epsilon is None else ", eps %.2f" % epsilon
        print("%d: done %d games, mean reward %.3f, speed %.2f f/s%s" % (
            frame, len(self.total_rewards), mean_reward, speed, epsilon_str
        ))
        # if (len(self.total_rewards) % 100) == 0:
        #     print("saving model at:", "model_checkout2/model_game_{}".format(len(self.total_rewards)))
        #     torch.save(self.agent, "model_checkout2/model_game_{}".format(len(self.total_rewards)))

        sys.stdout.flush()

        if mean_reward > self.stop_reward:
            print("Solved in %d frames!" % frame)
            # print("saving model at:", "model_checkout2/model_game_latest")
            # torch.save(self.agent, "model_checkout2/model_game_latest")
            return True
        return False

class RewardTracker:
    def __init__(self, writer, agent, stop_reward):
        self.writer = writer
        self.stop_reward = stop_reward
        self.agent = agent

    def __enter__(self):
        self.ts = time.time()
        self.ts_frame = 0
        self.total_rewards = []
        return self

    def __exit__(self, *args):
        self.writer.close()

    def reward(self, reward, frame, epsilon=None):
        self.total_rewards.append(reward)
        speed = (frame - self.ts_frame) / (time.time() - self.ts)
        self.ts_frame = frame
        self.ts = time.time()
        mean_reward = np.mean(self.total_rewards[-100:])
        std_reward = np.std(self.total_rewards[-100:])
        epsilon_str = "" if epsilon is None else ", eps %.2f" % epsilon
        print("%d: done %d games, mean reward %.3f,std reward %.3f, speed %.2f f/s%s" % (
            frame, len(self.total_rewards), mean_reward,std_reward, speed, epsilon_str
        ))
        # if (len(self.total_rewards) % 100) == 0:
        #     print("saving model at:", "model_checkout2/model_game_{}".format(len(self.total_rewards)))
        #     torch.save(self.agent, "model_checkout2/model_game_{}".format(len(self.total_rewards)))

        sys.stdout.flush()
        if epsilon is not None:
            self.writer.add_scalar("epsilon", epsilon, frame)
        self.writer.add_scalar("speed", speed, frame)
        self.writer.add_scalar("reward_100", mean_reward, frame)
        self.writer.add_scalar("reward", reward, frame)
        if mean_reward > self.stop_reward:
        #     print("Solved in %d frames!" % frame)
        #     print("saving model at:", "model_checkout2/model_game_latest")
        #     torch.save(self.agent, "model_checkout2/model_game_latest")
            return True
        return False


class AtariPGN(nn.Module):
    def __init__(self, input_shape, n_actions):
        super(AtariPGN, self).__init__()

        self.conv = nn.Sequential(
            nn.Conv2d(input_shape[0], 32, kernel_size=8, stride=4),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1),
            nn.ReLU()
        )

        conv_out_size = self._get_conv_out(input_shape)
        self.fc = nn.Sequential(
            nn.Linear(conv_out_size, 512),
            nn.ReLU(),
            nn.Linear(512, n_actions)
        )

    def _get_conv_out(self, shape):
        o = self.conv(torch.zeros(1, *shape))
        return int(np.prod(o.size()))

    def forward(self, x):
        fx = x.float() / 256
        conv_out = self.conv(fx).view(fx.size()[0], -1)
        return self.fc(conv_out)

